package modul.pkg2;

public class UKM {
    private String namaUnit;
    private Mahasiswa ketua;
    private Mahasiswa sekertaris;
    private Penduduk[] anggota;
    private String saya;
    

    public String getNamaUnit() {
        return namaUnit;
    }

    public void setNamaUnit(String namaUnit) {
        this.namaUnit = namaUnit;
    }

    public Mahasiswa getKetua() {
        return ketua;
    }

    public void setKetua(Mahasiswa ketua) {
        this.ketua = ketua;
    }

    public Mahasiswa getSekertaris() {
        return sekertaris;
    }

    public void setSekertaris(Mahasiswa sekertaris) {
        this.sekertaris = sekertaris;
    }

    public Penduduk[] getAnggota() {
        return anggota;
    }

    public void setAnggota(Penduduk[] anggota) {
        this.anggota = anggota;
    }
    
    

}
